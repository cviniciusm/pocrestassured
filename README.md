POC Rest-Assured

- JUnit 5
- Spring Boot 2.1.14
- Rest-assured 4.2.0

Based on [spring-mvc-webapp example](https://github.com/rest-assured/rest-assured/tree/rest-assured-4.3.0/examples/spring-mvc-webapp).