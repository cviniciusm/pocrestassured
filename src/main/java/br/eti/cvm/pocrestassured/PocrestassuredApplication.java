package br.eti.cvm.pocrestassured;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PocrestassuredApplication {

    public static void main(String[] args) {
        SpringApplication.run(PocrestassuredApplication.class, args);
    }

}
