package br.eti.cvm.pocrestassured.controller;

import br.eti.cvm.pocrestassured.model.Greeting;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

@RestController
@RequestMapping(value = "/secured")
public class GreetingController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @GetMapping("/greeting")
    public
    @ResponseBody
    Greeting greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name,
                      @AuthenticationPrincipal User user) {
        if (user == null || !user.getUsername().equals("username")) {
            throw new IllegalArgumentException("Not authorized");
        }

        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

}