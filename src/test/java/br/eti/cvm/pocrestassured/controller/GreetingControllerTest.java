package br.eti.cvm.pocrestassured.controller;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.WebApplicationContext;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@SpringBootTest
class GreetingControllerTest {

    @Autowired
    private WebApplicationContext context;

    @Test
    public void
    web_app_context_setup_allows_passing_in_mock_mvc_configurers_using_the_dsl() {
        RestAssuredMockMvc.given().
                webAppContextSetup(context, springSecurity()).
                postProcessors(httpBasic("username", "password")).
                param("name", "Johan").
                when().
                get("/secured/greeting").
                then().
                statusCode(200).
                body("content", equalTo("Hello, Johan!")).
                expect(authenticated().withUsername("username"));
    }

    @Test
    public void
    web_app_context_setup_allows_passing_in_mock_mvc_configurers_statically() {
        RestAssuredMockMvc.webAppContextSetup(context, springSecurity());

        try {
            RestAssuredMockMvc.given().
                    postProcessors(httpBasic("username", "password")).
                    param("name", "Johan").
                    when().
                    get("/secured/greeting").
                    then().
                    statusCode(200).
                    body("content", equalTo("Hello, Johan!")).
                    expect(authenticated().withUsername("username"));
        } finally {
            RestAssuredMockMvc.reset();
        }
    }
}